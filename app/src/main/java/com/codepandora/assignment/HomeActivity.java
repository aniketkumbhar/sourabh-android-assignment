package com.codepandora.assignment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codepandora.assignment.network.NetworkHandler;
import com.codepandora.assignment.network.model.APIResponse;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";

    private RelativeLayout relLoading;
    private ViewPager viewPager;
    private TextView txtRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();
        fetchDataFromApi();


    }

    private void init() {

        relLoading = (RelativeLayout) findViewById(R.id.relLoading);
        viewPager = (ViewPager) findViewById(R.id.pager);
        txtRetry = (TextView) findViewById(R.id.txtRetry);

        txtRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fetchDataFromApi();
            }
        });


    }

    private void fetchDataFromApi() {

        showProgress();

        NetworkHandler handler = new NetworkHandler(this);
        Call<APIResponse> call = handler.getApiService().getProfiles();
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Response<APIResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {

                    stopProgress();
                    APIResponse item = response.body();
                    showPager(item);

                } else {

                    showToast("Unknown error occurred!! Please try again..");
                    showFailure();
                    android.util.Log.e(TAG, "Response code - " + response.code());
                    android.util.Log.e(TAG, "Response message - " + response.message());
                }

            }

            @Override
            public void onFailure(Throwable t) {
                showToast("Server error occurred. Please try again!!");
                android.util.Log.e(TAG, t.getLocalizedMessage());
                showFailure();
            }
        });
    }

    private void showProgress() {

        relLoading.setVisibility(View.VISIBLE);
        txtRetry.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
    }

    private void stopProgress() {

        relLoading.setVisibility(View.GONE);
        txtRetry.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
    }

    private void showFailure() {

        relLoading.setVisibility(View.GONE);
        txtRetry.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);

    }

    private void showPager(APIResponse item) {

        HomeTabPagerAdapter adapter = new HomeTabPagerAdapter(getSupportFragmentManager(), item.getData().getChildren());
        viewPager.setAdapter(adapter);
    }


    private void showToast(String text) {

        Toast.makeText(HomeActivity.this, text, Toast.LENGTH_SHORT).show();

    }


}
