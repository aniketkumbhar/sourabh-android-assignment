package com.codepandora.assignment.network;

import com.codepandora.assignment.network.model.APIResponse;

import retrofit.Call;
import retrofit.http.GET;


public interface ApiService {
    @GET("funny.json")
    Call<APIResponse> getProfiles();
}
