package com.codepandora.assignment.network;

import android.content.Context;

import com.codepandora.assignment.Constants;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class NetworkHandler {


    private static ApiService mService;


    public NetworkHandler(Context context) {

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mService = restAdapter.create(ApiService.class);

    }

    public static ApiService getApiService() {
        return mService;
    }


}
