package com.codepandora.assignment.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class APIResponse {


    @SerializedName("kind")
    private String kind;
    @SerializedName("data")
    private Data data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public static class Data {

        @SerializedName("modhash")
        @Expose
        private String modhash;
        @SerializedName("children")
        @Expose
        private List<Child> children = new ArrayList<Child>();
        @SerializedName("after")
        @Expose
        private String after;
        @SerializedName("before")
        @Expose
        private Object before;


        public String getModhash() {
            return modhash;
        }


        public void setModhash(String modhash) {
            this.modhash = modhash;
        }


        public List<Child> getChildren() {
            return children;
        }


        public void setChildren(List<Child> children) {
            this.children = children;
        }


        public String getAfter() {
            return after;
        }


        public void setAfter(String after) {
            this.after = after;
        }


        public Object getBefore() {
            return before;
        }


        public void setBefore(Object before) {
            this.before = before;
        }

    }


    public static class Child {

        @SerializedName("kind")
        @Expose
        private String kind;
        @SerializedName("data")
        @Expose
        private Data_ data;


        public String getKind() {
            return kind;
        }


        public void setKind(String kind) {
            this.kind = kind;
        }


        public Data_ getData() {
            return data;
        }


        public void setData(Data_ data) {
            this.data = data;
        }

    }

    public class Data_ {

        @SerializedName("domain")
        @Expose
        private String domain;
        @SerializedName("banned_by")
        @Expose
        private Object bannedBy;
        @SerializedName("subreddit")
        @Expose
        private String subreddit;
        @SerializedName("selftext_html")
        @Expose
        private Object selftextHtml;
        @SerializedName("selftext")
        @Expose
        private String selftext;
        @SerializedName("likes")
        @Expose
        private Object likes;
        @SerializedName("suggested_sort")
        @Expose
        private Object suggestedSort;
        @SerializedName("user_reports")
        @Expose
        private List<Object> userReports = new ArrayList<Object>();
        @SerializedName("secure_media")
        @Expose
        private Object secureMedia;
        @SerializedName("link_flair_text")
        @Expose
        private Object linkFlairText;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("from_kind")
        @Expose
        private Object fromKind;
        @SerializedName("gilded")
        @Expose
        private int gilded;
        @SerializedName("archived")
        @Expose
        private Boolean archived;
        @SerializedName("clicked")
        @Expose
        private Boolean clicked;
        @SerializedName("report_reasons")
        @Expose
        private Object reportReasons;
        @SerializedName("author")
        @Expose
        private String author;
        @SerializedName("media")
        @Expose
        private Object media;
        @SerializedName("score")
        @Expose
        private int score;
        @SerializedName("approved_by")
        @Expose
        private Object approvedBy;
        @SerializedName("over_18")
        @Expose
        private Boolean over18;
        @SerializedName("hidden")
        @Expose
        private Boolean hidden;
        @SerializedName("preview")
        @Expose
        private Preview preview;
        @SerializedName("num_comments")
        @Expose
        private int numComments;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("subreddit_id")
        @Expose
        private String subredditId;
        @SerializedName("hide_score")
        @Expose
        private Boolean hideScore;
        @SerializedName("edited")
        @Expose
        private Boolean edited;
        @SerializedName("link_flair_css_class")
        @Expose
        private Object linkFlairCssClass;
        @SerializedName("author_flair_css_class")
        @Expose
        private Object authorFlairCssClass;
        @SerializedName("downs")
        @Expose
        private int downs;
        @SerializedName("saved")
        @Expose
        private Boolean saved;
        @SerializedName("removal_reason")
        @Expose
        private Object removalReason;
        @SerializedName("post_hint")
        @Expose
        private String postHint;
        @SerializedName("stickied")
        @Expose
        private Boolean stickied;
        @SerializedName("from")
        @Expose
        private Object from;
        @SerializedName("is_self")
        @Expose
        private Boolean isSelf;
        @SerializedName("from_id")
        @Expose
        private Object fromId;
        @SerializedName("permalink")
        @Expose
        private String permalink;
        @SerializedName("locked")
        @Expose
        private Boolean locked;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("created")
        @Expose
        private Double created;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("author_flair_text")
        @Expose
        private Object authorFlairText;
        @SerializedName("quarantine")
        @Expose
        private Boolean quarantine;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("created_utc")
        @Expose
        private Double createdUtc;
        @SerializedName("distinguished")
        @Expose
        private Object distinguished;
        @SerializedName("mod_reports")
        @Expose
        private List<Object> modReports = new ArrayList<Object>();
        @SerializedName("visited")
        @Expose
        private Boolean visited;
        @SerializedName("num_reports")
        @Expose
        private Object numReports;
        @SerializedName("ups")
        @Expose
        private int ups;


        public String getDomain() {
            return domain;
        }


        public void setDomain(String domain) {
            this.domain = domain;
        }


        public Object getBannedBy() {
            return bannedBy;
        }


        public void setBannedBy(Object bannedBy) {
            this.bannedBy = bannedBy;
        }


        public String getSubreddit() {
            return subreddit;
        }


        public void setSubreddit(String subreddit) {
            this.subreddit = subreddit;
        }


        public Object getSelftextHtml() {
            return selftextHtml;
        }


        public void setSelftextHtml(Object selftextHtml) {
            this.selftextHtml = selftextHtml;
        }


        public String getSelftext() {
            return selftext;
        }


        public void setSelftext(String selftext) {
            this.selftext = selftext;
        }


        public Object getLikes() {
            return likes;
        }


        public void setLikes(Object likes) {
            this.likes = likes;
        }


        public Object getSuggestedSort() {
            return suggestedSort;
        }


        public void setSuggestedSort(Object suggestedSort) {
            this.suggestedSort = suggestedSort;
        }


        public List<Object> getUserReports() {
            return userReports;
        }


        public void setUserReports(List<Object> userReports) {
            this.userReports = userReports;
        }


        public Object getSecureMedia() {
            return secureMedia;
        }


        public void setSecureMedia(Object secureMedia) {
            this.secureMedia = secureMedia;
        }


        public Object getLinkFlairText() {
            return linkFlairText;
        }


        public void setLinkFlairText(Object linkFlairText) {
            this.linkFlairText = linkFlairText;
        }


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }


        public Object getFromKind() {
            return fromKind;
        }


        public void setFromKind(Object fromKind) {
            this.fromKind = fromKind;
        }


        public int getGilded() {
            return gilded;
        }


        public void setGilded(int gilded) {
            this.gilded = gilded;
        }


        public Boolean getArchived() {
            return archived;
        }


        public void setArchived(Boolean archived) {
            this.archived = archived;
        }


        public Boolean getClicked() {
            return clicked;
        }


        public void setClicked(Boolean clicked) {
            this.clicked = clicked;
        }


        public Object getReportReasons() {
            return reportReasons;
        }


        public void setReportReasons(Object reportReasons) {
            this.reportReasons = reportReasons;
        }


        public String getAuthor() {
            return author;
        }


        public void setAuthor(String author) {
            this.author = author;
        }


        public Object getMedia() {
            return media;
        }


        public void setMedia(Object media) {
            this.media = media;
        }


        public int getScore() {
            return score;
        }


        public void setScore(int score) {
            this.score = score;
        }


        public Object getApprovedBy() {
            return approvedBy;
        }


        public void setApprovedBy(Object approvedBy) {
            this.approvedBy = approvedBy;
        }


        public Boolean getOver18() {
            return over18;
        }


        public void setOver18(Boolean over18) {
            this.over18 = over18;
        }


        public Boolean getHidden() {
            return hidden;
        }


        public void setHidden(Boolean hidden) {
            this.hidden = hidden;
        }


        public Preview getPreview() {
            return preview;
        }


        public void setPreview(Preview preview) {
            this.preview = preview;
        }


        public int getNumComments() {
            return numComments;
        }


        public void setNumComments(int numComments) {
            this.numComments = numComments;
        }


        public String getThumbnail() {
            return thumbnail;
        }


        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }


        public String getSubredditId() {
            return subredditId;
        }


        public void setSubredditId(String subredditId) {
            this.subredditId = subredditId;
        }


        public Boolean getHideScore() {
            return hideScore;
        }


        public void setHideScore(Boolean hideScore) {
            this.hideScore = hideScore;
        }


        public Boolean getEdited() {
            return edited;
        }


        public void setEdited(Boolean edited) {
            this.edited = edited;
        }


        public Object getLinkFlairCssClass() {
            return linkFlairCssClass;
        }


        public void setLinkFlairCssClass(Object linkFlairCssClass) {
            this.linkFlairCssClass = linkFlairCssClass;
        }


        public Object getAuthorFlairCssClass() {
            return authorFlairCssClass;
        }


        public void setAuthorFlairCssClass(Object authorFlairCssClass) {
            this.authorFlairCssClass = authorFlairCssClass;
        }


        public int getDowns() {
            return downs;
        }


        public void setDowns(int downs) {
            this.downs = downs;
        }


        public Boolean getSaved() {
            return saved;
        }


        public void setSaved(Boolean saved) {
            this.saved = saved;
        }


        public Object getRemovalReason() {
            return removalReason;
        }


        public void setRemovalReason(Object removalReason) {
            this.removalReason = removalReason;
        }


        public String getPostHint() {
            return postHint;
        }


        public void setPostHint(String postHint) {
            this.postHint = postHint;
        }


        public Boolean getStickied() {
            return stickied;
        }


        public void setStickied(Boolean stickied) {
            this.stickied = stickied;
        }


        public Object getFrom() {
            return from;
        }


        public void setFrom(Object from) {
            this.from = from;
        }


        public Boolean getIsSelf() {
            return isSelf;
        }


        public void setIsSelf(Boolean isSelf) {
            this.isSelf = isSelf;
        }


        public Object getFromId() {
            return fromId;
        }


        public void setFromId(Object fromId) {
            this.fromId = fromId;
        }


        public String getPermalink() {
            return permalink;
        }


        public void setPermalink(String permalink) {
            this.permalink = permalink;
        }


        public Boolean getLocked() {
            return locked;
        }


        public void setLocked(Boolean locked) {
            this.locked = locked;
        }


        public String getName() {
            return name;
        }


        public void setName(String name) {
            this.name = name;
        }


        public Double getCreated() {
            return created;
        }


        public void setCreated(Double created) {
            this.created = created;
        }


        public String getUrl() {
            return url;
        }


        public void setUrl(String url) {
            this.url = url;
        }


        public Object getAuthorFlairText() {
            return authorFlairText;
        }


        public void setAuthorFlairText(Object authorFlairText) {
            this.authorFlairText = authorFlairText;
        }


        public Boolean getQuarantine() {
            return quarantine;
        }


        public void setQuarantine(Boolean quarantine) {
            this.quarantine = quarantine;
        }


        public String getTitle() {
            return title;
        }


        public void setTitle(String title) {
            this.title = title;
        }


        public Double getCreatedUtc() {
            return createdUtc;
        }


        public void setCreatedUtc(Double createdUtc) {
            this.createdUtc = createdUtc;
        }


        public Object getDistinguished() {
            return distinguished;
        }


        public void setDistinguished(Object distinguished) {
            this.distinguished = distinguished;
        }


        public List<Object> getModReports() {
            return modReports;
        }


        public void setModReports(List<Object> modReports) {
            this.modReports = modReports;
        }


        public Boolean getVisited() {
            return visited;
        }


        public void setVisited(Boolean visited) {
            this.visited = visited;
        }


        public Object getNumReports() {
            return numReports;
        }


        public void setNumReports(Object numReports) {
            this.numReports = numReports;
        }


        public int getUps() {
            return ups;
        }


        public void setUps(int ups) {
            this.ups = ups;
        }

    }


    public static class Preview {

        @SerializedName("images")
        @Expose
        private List<Image> images = new ArrayList<Image>();


        public List<Image> getImages() {
            return images;
        }


        public void setImages(List<Image> images) {
            this.images = images;
        }

    }

    public static class Image {

        @SerializedName("source")
        @Expose
        private Source source;
        @SerializedName("resolutions")
        @Expose
        private List<Resolution> resolutions = new ArrayList<Resolution>();
        @SerializedName("id")
        @Expose
        private String id;


        public Source getSource() {
            return source;
        }


        public void setSource(Source source) {
            this.source = source;
        }


        public List<Resolution> getResolutions() {
            return resolutions;
        }


        public void setResolutions(List<Resolution> resolutions) {
            this.resolutions = resolutions;
        }


        public String getId() {
            return id;
        }


        public void setId(String id) {
            this.id = id;
        }

    }


    public static class Source {

        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private int width;
        @SerializedName("height")
        @Expose
        private int height;


        public String getUrl() {
            return url;
        }


        public void setUrl(String url) {
            this.url = url;
        }


        public int getWidth() {
            return width;
        }


        public void setWidth(int width) {
            this.width = width;
        }


        public int getHeight() {
            return height;
        }


        public void setHeight(int height) {
            this.height = height;
        }

    }


    public static class Resolution {

        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private int width;
        @SerializedName("height")
        @Expose
        private int height;


        public String getUrl() {
            return url;
        }


        public void setUrl(String url) {
            this.url = url;
        }


        public int getWidth() {
            return width;
        }


        public void setWidth(int width) {
            this.width = width;
        }


        public int getHeight() {
            return height;
        }


        public void setHeight(int height) {
            this.height = height;
        }

    }
}
