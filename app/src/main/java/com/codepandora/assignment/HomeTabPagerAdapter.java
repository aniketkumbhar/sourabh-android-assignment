package com.codepandora.assignment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.codepandora.assignment.network.model.APIResponse;
import com.google.gson.Gson;

import java.util.List;


public class HomeTabPagerAdapter extends FragmentPagerAdapter {


    private List<APIResponse.Child> itemList;

    public HomeTabPagerAdapter(FragmentManager supportFragmentManager, List<APIResponse.Child> children) {
        super(supportFragmentManager);
        this.itemList = children;
    }


    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Fragment getItem(int position) {

        APIResponse.Child eachItem = itemList.get(position);
        Bundle bundle = new Bundle();
        bundle.putString("item", new Gson().toJson(eachItem));
        HomePagerFragment fragment = new HomePagerFragment();
        fragment.setArguments(bundle);
        return fragment;

    }


}