package com.codepandora.assignment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codepandora.assignment.network.model.APIResponse;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


public class HomePagerFragment extends Fragment {


    private TextView txtTitle;
    private TextView txtAuthor;
    private ImageView imgView;

    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public HomePagerFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_home_pager, container, false);


        initImageLoader();
        init(view);
        return view;
    }

    private void initImageLoader() {

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .displayer(new FadeInBitmapDisplayer(1000))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
    }

    private void init(View view) {


        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtAuthor = (TextView) view.findViewById(R.id.txtAuthor);
        imgView = (ImageView) view.findViewById(R.id.imgView);

        Bundle bundle = getArguments();
        String strItem = bundle.getString("item");
        APIResponse.Child eachItem = new Gson().fromJson(strItem, APIResponse.Child.class);

        String title = eachItem.getData().getTitle();
        String author = eachItem.getData().getAuthor();
        String imageUrl = eachItem.getData().getThumbnail();
        String postUrl = eachItem.getData().getUrl();
        txtTitle.setTag(postUrl);

        imageLoader.displayImage(imageUrl, imgView, options);

        if (!TextUtils.isEmpty(title)) {

            txtTitle.setText(title);
        }
        if (!TextUtils.isEmpty(author)) {

            txtAuthor.setText(author);
        }


        txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = (String) view.getTag();
                if (!TextUtils.isEmpty(url)) {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(i);
                }

            }
        });


    }


}
